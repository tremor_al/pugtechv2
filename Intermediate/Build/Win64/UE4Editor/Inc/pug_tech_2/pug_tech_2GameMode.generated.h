// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PUG_TECH_2_pug_tech_2GameMode_generated_h
#error "pug_tech_2GameMode.generated.h already included, missing '#pragma once' in pug_tech_2GameMode.h"
#endif
#define PUG_TECH_2_pug_tech_2GameMode_generated_h

#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_RPC_WRAPPERS
#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesApug_tech_2GameMode(); \
	friend PUG_TECH_2_API class UClass* Z_Construct_UClass_Apug_tech_2GameMode(); \
public: \
	DECLARE_CLASS(Apug_tech_2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/pug_tech_2"), PUG_TECH_2_API) \
	DECLARE_SERIALIZER(Apug_tech_2GameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesApug_tech_2GameMode(); \
	friend PUG_TECH_2_API class UClass* Z_Construct_UClass_Apug_tech_2GameMode(); \
public: \
	DECLARE_CLASS(Apug_tech_2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/pug_tech_2"), PUG_TECH_2_API) \
	DECLARE_SERIALIZER(Apug_tech_2GameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PUG_TECH_2_API Apug_tech_2GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Apug_tech_2GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PUG_TECH_2_API, Apug_tech_2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Apug_tech_2GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PUG_TECH_2_API Apug_tech_2GameMode(Apug_tech_2GameMode&&); \
	PUG_TECH_2_API Apug_tech_2GameMode(const Apug_tech_2GameMode&); \
public:


#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PUG_TECH_2_API Apug_tech_2GameMode(Apug_tech_2GameMode&&); \
	PUG_TECH_2_API Apug_tech_2GameMode(const Apug_tech_2GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PUG_TECH_2_API, Apug_tech_2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Apug_tech_2GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Apug_tech_2GameMode)


#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_9_PROLOG
#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_RPC_WRAPPERS \
	pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_INCLASS \
	pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_INCLASS_NO_PURE_DECLS \
	pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID pugtechv2_Source_pug_tech_2_pug_tech_2GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
