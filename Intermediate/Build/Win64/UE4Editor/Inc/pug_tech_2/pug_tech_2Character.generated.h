// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PUG_TECH_2_pug_tech_2Character_generated_h
#error "pug_tech_2Character.generated.h already included, missing '#pragma once' in pug_tech_2Character.h"
#endif
#define PUG_TECH_2_pug_tech_2Character_generated_h

#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSwipeRight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SwipeRight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSwipeLeft) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SwipeLeft(); \
		P_NATIVE_END; \
	}


#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSwipeRight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SwipeRight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSwipeLeft) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SwipeLeft(); \
		P_NATIVE_END; \
	}


#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesApug_tech_2Character(); \
	friend PUG_TECH_2_API class UClass* Z_Construct_UClass_Apug_tech_2Character(); \
public: \
	DECLARE_CLASS(Apug_tech_2Character, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/pug_tech_2"), NO_API) \
	DECLARE_SERIALIZER(Apug_tech_2Character) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesApug_tech_2Character(); \
	friend PUG_TECH_2_API class UClass* Z_Construct_UClass_Apug_tech_2Character(); \
public: \
	DECLARE_CLASS(Apug_tech_2Character, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/pug_tech_2"), NO_API) \
	DECLARE_SERIALIZER(Apug_tech_2Character) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Apug_tech_2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Apug_tech_2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Apug_tech_2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Apug_tech_2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Apug_tech_2Character(Apug_tech_2Character&&); \
	NO_API Apug_tech_2Character(const Apug_tech_2Character&); \
public:


#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Apug_tech_2Character(Apug_tech_2Character&&); \
	NO_API Apug_tech_2Character(const Apug_tech_2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Apug_tech_2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Apug_tech_2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Apug_tech_2Character)


#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(Apug_tech_2Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(Apug_tech_2Character, FollowCamera); } \
	FORCEINLINE static uint32 __PPO__Capsule() { return STRUCT_OFFSET(Apug_tech_2Character, Capsule); }


#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_9_PROLOG
#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_RPC_WRAPPERS \
	pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_INCLASS \
	pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_INCLASS_NO_PURE_DECLS \
	pugtechv2_Source_pug_tech_2_pug_tech_2Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID pugtechv2_Source_pug_tech_2_pug_tech_2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
