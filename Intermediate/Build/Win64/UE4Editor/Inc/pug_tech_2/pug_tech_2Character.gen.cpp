// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "pug_tech_2Character.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodepug_tech_2Character() {}
// Cross Module References
	PUG_TECH_2_API UClass* Z_Construct_UClass_Apug_tech_2Character_NoRegister();
	PUG_TECH_2_API UClass* Z_Construct_UClass_Apug_tech_2Character();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_pug_tech_2();
	PUG_TECH_2_API UFunction* Z_Construct_UFunction_Apug_tech_2Character_SwipeLeft();
	PUG_TECH_2_API UFunction* Z_Construct_UFunction_Apug_tech_2Character_SwipeRight();
	ENGINE_API UClass* Z_Construct_UClass_UCapsuleComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
// End Cross Module References
	void Apug_tech_2Character::StaticRegisterNativesApug_tech_2Character()
	{
		UClass* Class = Apug_tech_2Character::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SwipeLeft", &Apug_tech_2Character::execSwipeLeft },
			{ "SwipeRight", &Apug_tech_2Character::execSwipeRight },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_Apug_tech_2Character_SwipeLeft()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_Apug_tech_2Character, "SwipeLeft", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_Apug_tech_2Character_SwipeRight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_Apug_tech_2Character, "SwipeRight", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_Apug_tech_2Character_NoRegister()
	{
		return Apug_tech_2Character::StaticClass();
	}
	UClass* Z_Construct_UClass_Apug_tech_2Character()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_ACharacter,
				(UObject* (*)())Z_Construct_UPackage__Script_pug_tech_2,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_Apug_tech_2Character_SwipeLeft, "SwipeLeft" }, // 519538272
				{ &Z_Construct_UFunction_Apug_tech_2Character_SwipeRight, "SwipeRight" }, // 125030362
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Navigation" },
				{ "IncludePath", "pug_tech_2Character.h" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftTrace_MetaData[] = {
				{ "Category", "Player" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
			auto NewProp_LeftTrace_SetBit = [](void* Obj){ ((Apug_tech_2Character*)Obj)->LeftTrace = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_LeftTrace = { UE4CodeGen_Private::EPropertyClass::Bool, "LeftTrace", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Apug_tech_2Character), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_LeftTrace_SetBit)>::SetBit, METADATA_PARAMS(NewProp_LeftTrace_MetaData, ARRAY_COUNT(NewProp_LeftTrace_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightTrace_MetaData[] = {
				{ "Category", "Player" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
			auto NewProp_RightTrace_SetBit = [](void* Obj){ ((Apug_tech_2Character*)Obj)->RightTrace = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_RightTrace = { UE4CodeGen_Private::EPropertyClass::Bool, "RightTrace", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Apug_tech_2Character), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_RightTrace_SetBit)>::SetBit, METADATA_PARAMS(NewProp_RightTrace_MetaData, ARRAY_COUNT(NewProp_RightTrace_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Move_Left_Limit_MetaData[] = {
				{ "Category", "Player" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Move_Left_Limit = { UE4CodeGen_Private::EPropertyClass::Float, "Move_Left_Limit", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(Apug_tech_2Character, Move_Left_Limit), METADATA_PARAMS(NewProp_Move_Left_Limit_MetaData, ARRAY_COUNT(NewProp_Move_Left_Limit_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Move_Right_Limit_MetaData[] = {
				{ "Category", "Player" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Move_Right_Limit = { UE4CodeGen_Private::EPropertyClass::Float, "Move_Right_Limit", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(Apug_tech_2Character, Move_Right_Limit), METADATA_PARAMS(NewProp_Move_Right_Limit_MetaData, ARRAY_COUNT(NewProp_Move_Right_Limit_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsMovingLeftOrRight_MetaData[] = {
				{ "Category", "Player" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
			auto NewProp_IsMovingLeftOrRight_SetBit = [](void* Obj){ ((Apug_tech_2Character*)Obj)->IsMovingLeftOrRight = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsMovingLeftOrRight = { UE4CodeGen_Private::EPropertyClass::Bool, "IsMovingLeftOrRight", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Apug_tech_2Character), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_IsMovingLeftOrRight_SetBit)>::SetBit, METADATA_PARAMS(NewProp_IsMovingLeftOrRight_MetaData, ARRAY_COUNT(NewProp_IsMovingLeftOrRight_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CanMoveLeftOrRight_MetaData[] = {
				{ "Category", "Player" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
			auto NewProp_CanMoveLeftOrRight_SetBit = [](void* Obj){ ((Apug_tech_2Character*)Obj)->CanMoveLeftOrRight = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_CanMoveLeftOrRight = { UE4CodeGen_Private::EPropertyClass::Bool, "CanMoveLeftOrRight", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Apug_tech_2Character), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_CanMoveLeftOrRight_SetBit)>::SetBit, METADATA_PARAMS(NewProp_CanMoveLeftOrRight_MetaData, ARRAY_COUNT(NewProp_CanMoveLeftOrRight_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VelocityMovement_MetaData[] = {
				{ "Category", "Player" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
				{ "ToolTip", "Velocity Modifier" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VelocityMovement = { UE4CodeGen_Private::EPropertyClass::Float, "VelocityMovement", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(Apug_tech_2Character, VelocityMovement), METADATA_PARAMS(NewProp_VelocityMovement_MetaData, ARRAY_COUNT(NewProp_VelocityMovement_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseLookUpRate_MetaData[] = {
				{ "Category", "Player" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
				{ "ToolTip", "Base look up/down rate, in deg/sec. Other scaling may affect final rate." },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseLookUpRate = { UE4CodeGen_Private::EPropertyClass::Float, "BaseLookUpRate", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000020005, 1, nullptr, STRUCT_OFFSET(Apug_tech_2Character, BaseLookUpRate), METADATA_PARAMS(NewProp_BaseLookUpRate_MetaData, ARRAY_COUNT(NewProp_BaseLookUpRate_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTurnRate_MetaData[] = {
				{ "Category", "Player" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
				{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseTurnRate = { UE4CodeGen_Private::EPropertyClass::Float, "BaseTurnRate", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000020005, 1, nullptr, STRUCT_OFFSET(Apug_tech_2Character, BaseTurnRate), METADATA_PARAMS(NewProp_BaseTurnRate_MetaData, ARRAY_COUNT(NewProp_BaseTurnRate_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Capsule_MetaData[] = {
				{ "AllowPrivateAccess", "true" },
				{ "Category", "Capsule" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Capsule = { UE4CodeGen_Private::EPropertyClass::Object, "Capsule", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a000d, 1, nullptr, STRUCT_OFFSET(Apug_tech_2Character, Capsule), Z_Construct_UClass_UCapsuleComponent_NoRegister, METADATA_PARAMS(NewProp_Capsule_MetaData, ARRAY_COUNT(NewProp_Capsule_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[] = {
				{ "AllowPrivateAccess", "true" },
				{ "Category", "Camera" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
				{ "ToolTip", "Follow camera" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FollowCamera = { UE4CodeGen_Private::EPropertyClass::Object, "FollowCamera", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(Apug_tech_2Character, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(NewProp_FollowCamera_MetaData, ARRAY_COUNT(NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[] = {
				{ "AllowPrivateAccess", "true" },
				{ "Category", "Camera" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "pug_tech_2Character.h" },
				{ "ToolTip", "Camera boom positioning the camera behind the character" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom = { UE4CodeGen_Private::EPropertyClass::Object, "CameraBoom", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(Apug_tech_2Character, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(NewProp_CameraBoom_MetaData, ARRAY_COUNT(NewProp_CameraBoom_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_LeftTrace,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_RightTrace,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Move_Left_Limit,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Move_Right_Limit,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_IsMovingLeftOrRight,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CanMoveLeftOrRight,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_VelocityMovement,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_BaseLookUpRate,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_BaseTurnRate,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Capsule,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FollowCamera,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CameraBoom,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<Apug_tech_2Character>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&Apug_tech_2Character::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00800080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Apug_tech_2Character, 561859450);
	static FCompiledInDefer Z_CompiledInDefer_UClass_Apug_tech_2Character(Z_Construct_UClass_Apug_tech_2Character, &Apug_tech_2Character::StaticClass, TEXT("/Script/pug_tech_2"), TEXT("Apug_tech_2Character"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Apug_tech_2Character);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
