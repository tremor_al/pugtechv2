// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PUG_TECH_2_FloorTile_generated_h
#error "FloorTile.generated.h already included, missing '#pragma once' in FloorTile.h"
#endif
#define PUG_TECH_2_FloorTile_generated_h

#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_RPC_WRAPPERS
#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFloorTile(); \
	friend PUG_TECH_2_API class UClass* Z_Construct_UClass_AFloorTile(); \
public: \
	DECLARE_CLASS(AFloorTile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/pug_tech_2"), NO_API) \
	DECLARE_SERIALIZER(AFloorTile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFloorTile(); \
	friend PUG_TECH_2_API class UClass* Z_Construct_UClass_AFloorTile(); \
public: \
	DECLARE_CLASS(AFloorTile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/pug_tech_2"), NO_API) \
	DECLARE_SERIALIZER(AFloorTile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFloorTile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFloorTile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloorTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloorTile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloorTile(AFloorTile&&); \
	NO_API AFloorTile(const AFloorTile&); \
public:


#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloorTile(AFloorTile&&); \
	NO_API AFloorTile(const AFloorTile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloorTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloorTile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFloorTile)


#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_PRIVATE_PROPERTY_OFFSET
#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_9_PROLOG
#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_PRIVATE_PROPERTY_OFFSET \
	pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_RPC_WRAPPERS \
	pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_INCLASS \
	pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_PRIVATE_PROPERTY_OFFSET \
	pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_INCLASS_NO_PURE_DECLS \
	pugtechv2_Source_pug_tech_2_Public_FloorTile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID pugtechv2_Source_pug_tech_2_Public_FloorTile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
