// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "pug_tech_2GameMode.h"
#include "pug_tech_2Character.h"
#include "UObject/ConstructorHelpers.h"

Apug_tech_2GameMode::Apug_tech_2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
